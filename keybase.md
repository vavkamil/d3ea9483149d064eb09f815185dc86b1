### Keybase proof

I hereby claim:

  * I am vavkamil on github.
  * I am kamil_kiwi (https://keybase.io/kamil_kiwi) on keybase.
  * I have a public key ASDQMrxWDIIThXhqFK9NJhP6hZTdrO5MjI7M_T4bBsTbsAo

To claim this, I am signing this object:

```json
{
  "body": {
    "key": {
      "eldest_kid": "0120d032bc560c821385786a14af4d2613fa8594ddacee4c8c8eccfd3e1b06c4dbb00a",
      "host": "keybase.io",
      "kid": "0120d032bc560c821385786a14af4d2613fa8594ddacee4c8c8eccfd3e1b06c4dbb00a",
      "uid": "67f89a99c3301317de6a82b6944e6f19",
      "username": "kamil_kiwi"
    },
    "merkle_root": {
      "ctime": 1568460808,
      "hash": "0d0d839e96ab9a907575a13d1000a8b6e7025e9aae56610cbbce2eb7d2f4a0f44ff0927471b2bf3f6ba903611e600f53aa3a56556c018742cf867bb64c1d897a",
      "hash_meta": "67ce59bcf1b046b3554088cf87de4b55a531f881a3b26eceb64f4a86b71f0ebb",
      "seqno": 7294900
    },
    "service": {
      "entropy": "NBflQt/wZsZNi5CdLe9n1uYI",
      "name": "github",
      "username": "vavkamil"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "4.4.1"
  },
  "ctime": 1568460815,
  "expire_in": 504576000,
  "prev": "dd74c258ecb634fd49efa6e7831d403c24b92578afa55d021da50cd1efccd768",
  "seqno": 6,
  "tag": "signature"
}
```

with the key [ASDQMrxWDIIThXhqFK9NJhP6hZTdrO5MjI7M_T4bBsTbsAo](https://keybase.io/kamil_kiwi), yielding the signature:

```
hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEg0DK8VgyCE4V4ahSvTSYT+oWU3azuTIyOzP0+GwbE27AKp3BheWxvYWTESpcCBsQg3XTCWOy2NP1J76bngx1APCS5JXivpV0CHaUM0e/M12jEIOSt/Wkdpa8UDACtg2yvbW6ak18OfY4Fxzp7zeZNXJUaAgHCo3NpZ8RAFQt0XCOkxbchFvhBCtcyD79Ojq+sZXUmYjFyrxSOBn1yccsPdFHWzWsDZwcTSzfaMLP49AgNtW5vMo/5y+THAahzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIJzXNdQAbZbmUgsuC5sssT2YotwQpkKjXJc+JJPvZs+Oo3RhZ80CAqd2ZXJzaW9uAQ==

```

And finally, I am proving ownership of the github account by posting this as a gist.

### My publicly-auditable identity:

https://keybase.io/kamil_kiwi

### From the command line:

Consider the [keybase command line program](https://keybase.io/download).

```bash
# look me up
keybase id kamil_kiwi
```